import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;

import static org.junit.Assert.assertEquals;

public class TestExample {

    private Lottery lottery;
    private ArrayList<String> arrayList = new ArrayList<String>();

    @Before
    public void setup(){
        lottery = new Lottery(0, 999999);
        arrayList.add("000000");
        arrayList.add("111111");
        arrayList.add("555555");
    }

    @Test(timeout = 3000, expected = NumberFormatException.class)
    public void testSpeed(){
        lottery.calculate();
    }

    @Test
    public void testHardMethod(){
        assertEquals(3, lottery.hardMethod(arrayList));
    }

    @After
    public void clean(){
        arrayList.clear();
    }


}
