import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        int taskNumber = 0;
        System.out.println("Доступные задачи:");
        System.out.println("№1: Шахматная доска");
        System.out.println("№6: Счаслтивые билеты");
        System.out.print("Введите номер задачи:");

        do {
            Scanner scanner = new Scanner(System.in);
            String numberOfTask = scanner.nextLine();

            try {
                taskNumber = Integer.parseInt(numberOfTask);
                if (taskNumber != 1 && taskNumber != 6) {
                    taskNumber = 0;
                    System.out.print("Задачи с этим номер нет, повторите:");
                }
            } catch (NumberFormatException ex) {
                System.out.print("Вы ввели неверный номер, повторите:");
            }

        } while (taskNumber == 0);

        switch (taskNumber) {
            case 1:
                chessTask();
                break;
            case 6:
                happyTicketTask();
                break;
        }

    }

    // Chess desk

    private static void chessTask() {
        System.out.println("\nЗадача \"Шахматная доска\"");
        System.out.println("----------------------------------------\n");

        Scanner scanner = new Scanner(System.in);

        int height, width;

        do {
            System.out.print("Укажите ширину: ");
            width = numberReader(scanner);

            System.out.print("Укажите высоту: ");
            height = numberReader(scanner);

        } while (height <= 0 || width <= 0);

        Chess chess = new Chess(height, width);
        chess.build();
    }

    private static int numberReader(Scanner scanner) {
        int selectedNumber = 0;

        boolean isError = true;

        do {
            String numberString = scanner.nextLine();

            try {
                selectedNumber = Integer.parseInt(numberString);

                if (selectedNumber < 0) {
                    System.out.print("Число не должно быть отрицательным, повторите: ");
                } else if (selectedNumber == 0) {
                    System.out.print("Число не должно быть равно нулю, повторите: ");
                } else {
                    isError = false;
                }

            } catch (Exception ex) {
                // ex.printStackTrace();
                System.out.print("Ошибка, введите обычное число без знаков: ");
            }

        } while (isError);

        return selectedNumber;
    }

    /// Happy Ticket

    private static void happyTicketTask() {
        System.out.println("\nДобро пожаловать в \"Счасливые билеты!\"");
        System.out.println("----------------------------------------\n");

        Scanner scanner = new Scanner(System.in);

        int startNumber, endNumber;

        do {
            System.out.print("Укажите минимальный номер билета: ");
            startNumber = countReader(scanner);

            System.out.print("Укажите максимальный номер билета: ");
            endNumber = countReader(scanner);

            if (startNumber >= endNumber) {
                System.out.println("Ошибка! Максимальное число билета должно быть больше минимального!");
            }

        } while (startNumber >= endNumber);

        Lottery mLottery = new Lottery(startNumber, endNumber);
        mLottery.calculate();
    }

    private static int countReader(Scanner scanner) {
        int selectedNumber = 0;

        boolean isError = true;

        do {
            // "001024"
            String numberString = scanner.nextLine();

            try {
                // 1024
                selectedNumber = Integer.parseInt(numberString);

                if (selectedNumber < 0) {
                    System.out.print("Число не должно быть отрицательным, повторите: ");
                } else if (numberString.length() != 6) {
                    System.out.print("Число долно быть длиной в 6 символов(000000 - 999999), повторите: ");
                } else {
                    isError = false;
                }

            } catch (NumberFormatException ex) {
                // ex.printStackTrace();
                System.out.print("Ошибка, введите обычное число без знаков: ");
            }

        } while (isError);

        return selectedNumber;
    }

}
